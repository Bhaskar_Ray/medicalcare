-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2014 at 05:00 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bhaskar`
--

-- --------------------------------------------------------

--
-- Table structure for table `adlogin_tbl`
--

CREATE TABLE IF NOT EXISTS `adlogin_tbl` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adname` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `adlogin_tbl`
--

INSERT INTO `adlogin_tbl` (`id`, `adname`, `pwd`) VALUES
(1, 'Estiak', '123456'),
(2, 'Amit', '01830103727'),
(3, 'Bhaskar', '01673533450');

-- --------------------------------------------------------

--
-- Table structure for table `ambulance_tbl`
--

CREATE TABLE IF NOT EXISTS `ambulance_tbl` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `ambulance` varchar(1000) NOT NULL,
  `amcontact` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ambulance_tbl`
--

INSERT INTO `ambulance_tbl` (`id`, `ambulance`, `amcontact`) VALUES
(3, 'Alfa Private Medical Service', '\r\n<p>\r\n47, Panchalaish R/A</br>\r\nChittagong</br>\r\nTelephone</br>\r\n031-2551111,01819-371919,01819-327070, 01919-371919</p>\r\n'),
(4, 'Alif Ambulance Service', '<p>47, Panchlaish R/A</br>\r\nChittagong</br>\r\nTelephone</br>\r\n031-657574, 01819-325060,01977325060</p>\r\n'),
(6, 'General Hospital', '<p>Chittagong</br>\r\nPhone:031-619584</p>'),
(7, 'Holy Crescent Hospital Ambulance Service', '<p>Chittagong</br>\r\nPhone:031-620025</p>\r\n '),
(8, 'Media Amb Service', '<p>Chittagong</br>\r\nPhone: 031-650000</p>'),
(9, 'Modern Ambulance Service', '<p>20, K.B. Fazlul Quader Road,</br>\r\nChittagong</br>\r\nPhone:031-639730, 0154633214, 01716074497</p>\r\n '),
(10, 'Poil Hospital Ambulance Service', '<p>Chittagong</br>\r\nPhone:031-502024, 505021-9<p>\r\n '),
(11, 'Railway Hospital Ambulance Service', '<p>Chittagong</br>\r\nPhone:031-502220</p>\r\n ');

-- --------------------------------------------------------

--
-- Table structure for table `bhaskar_tbl`
--

CREATE TABLE IF NOT EXISTS `bhaskar_tbl` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `dname` varchar(30) NOT NULL,
  `dage` int(3) NOT NULL,
  `dgroup` varchar(30) NOT NULL,
  `dmobile` varchar(25) NOT NULL,
  `darea` varchar(30) NOT NULL,
  `dgender` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `bhaskar_tbl`
--

INSERT INTO `bhaskar_tbl` (`id`, `dname`, `dage`, `dgroup`, `dmobile`, `darea`, `dgender`) VALUES
(15, 'Pharhad', 21, 'B-', '01672898562', 'Halishahar', 'Male'),
(16, 'Muhammad Estiak Omar', 22, 'AB+', '01710200333', 'Agrabad', 'Male'),
(17, 'Amit Chowdhuy', 23, 'A+', '01830103727', '2 No Gate', 'Male'),
(18, 'Moinul', 23, 'O-', '01673661388', 'Dewanhat', 'Male'),
(20, 'Shihab Uddin', 21, 'AB+', '01674562321', 'Battery Golli', 'Male'),
(21, 'Jakaria Masud', 22, 'A+', '01823741982', 'Oxygen', 'Male'),
(22, 'Shahedul Alam', 22, 'O+', '01831440206', 'Hathajari', 'Male'),
(23, 'Atiquer Rahman', 22, 'B+', '01840877727', 'Bakolia', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_tbl`
--

CREATE TABLE IF NOT EXISTS `doctor_tbl` (
  `mem_id` int(8) NOT NULL AUTO_INCREMENT,
  `docname` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `qualification` varchar(100) NOT NULL,
  `chamber` varchar(1000) NOT NULL,
  `speciality` varchar(100) NOT NULL,
  PRIMARY KEY (`mem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `doctor_tbl`
--

INSERT INTO `doctor_tbl` (`mem_id`, `docname`, `designation`, `qualification`, `chamber`, `speciality`) VALUES
(5, 'Dr. MD. Rezaul Karim', 'Professor & Departmental Head of USTC', 'MBBS, MCPS, DGO, FCPS', 'CSCR 1675/A O. R. Nizam Road. Room No-405,3rd Floor.Contac No:01812448233', 'General Surgery'),
(9, 'Professor Dr. MA. A. Foyez', 'Professor(Medicine). Sir Sallimullah Medical College.', 'FCPS(Medicine),FRCP,PHD(UK),Fellow of WHO(Neuromedicine-UK)', 'CSCR 1675/A O. R. Nizam Road. Room No-405, 3rd Floor. Contac No: 01812448233.', 'General Medicine'),
(10, 'Professor Dr. Mahabub Kamal Chowdhury.', 'Professor(Medicine)', 'MBBS, FCPS.', 'Room NO: 305, CSCR O. R. Nizam Road. ', 'General Medicine'),
(11, 'Professor Dr. Imran Bin Yunus.', 'Professor', '<p>MBBS</br>FCPS</br>FRCP</br></p>', ' <p>Room NO: 305</br>CSCR O.R.Nizam Road.</p>', 'Nephrology');

-- --------------------------------------------------------

--
-- Table structure for table `email_data`
--

CREATE TABLE IF NOT EXISTS `email_data` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `uname` varchar(100) NOT NULL,
  `emailID` varchar(100) NOT NULL,
  `umobile` varchar(100) NOT NULL,
  `pass` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `email_data`
--

INSERT INTO `email_data` (`id`, `uname`, `emailID`, `umobile`, `pass`) VALUES
(2, 'Estiak Omar', 'mdestiakomar@gmail.com', '01723400678', 'vzB-#2'),
(3, 'Estiak Omar', 'mdestiakomar@gmail.com', '01723400678', 'wE?uHl'),
(4, 'Estiak Omar', 'mdestiakomar@gmail.com', '01723400678', 'NBIjA6'),
(5, 'Bhaskar Ray', 'raybhaskar.ray@gmail.com', '01673533450', '3vg5SF'),
(6, 'Estiak Omar', 'mdestiakomar@gmail.com', '01710200333', 'KOOp?W'),
(7, 'Bhaskar Ray', 'raybhaskar.ray@gmail.com', '01673533450', 'rs-cC8'),
(8, 'Bhaskar', 'raybhaskar.ray@gmail.com', '01673533450', 'TI4E-B'),
(9, 'Bhaskar', 'raybhaskar.ray@gmail.com', '01673533450', 'H8QS5p'),
(10, 'Estiak Omar', 'mdestiakomar@gmail.com', '01712200333', 'V9cZnt'),
(11, 'Estiak Omar', 'mdestiakomar@gmail.com', '01712200333', '#WR81M'),
(12, 'Bhaskar', 'raybhaskar.ray@gmail.com', '01673533450', '3hQoZ+'),
(13, 'Estiak Omar', 'mdestiakomar@gmail.com', '01710200333', '1aco8K'),
(14, 'Estiak Omar', 'mdestiakomar@gmail.com', '01710200333', 'MTelPN'),
(15, 'Bhaskar', 'raybhaskar.ray@gmail.com', '01723393225', 'kD5yEe'),
(16, 'Estik', 'mdestiakomar@gmail.com', '01710200333', 'f#Z8fF'),
(17, 'Estiak', 'mdestiakomar@gmail.com', '01710200333', 'DPR?rv'),
(18, 'Estiak', 'mdestiakomar@gmail.com', '01710200333', 'DS+4fk'),
(19, 'Estiak', 'mdestiakomar@gmail.com', '01710200333', '&yP4jD'),
(20, 'Estiak', 'mdestiakomar@gmail.com', '01720200333', 'u7r+NF'),
(21, 'Estiak', 'mdestiakomar@gmail.com', '01710200333', 's9kpmF'),
(22, 'Bhaskar', 'raybhaskar.ray@gmail.com', '01673533450', 'qV#JNu'),
(23, 'Amit', 'amitchowdhury565@gmail.com', '01830103727', '5fvouc'),
(24, 'Esti', 'estiak.omar@yahoo.com', '02993', 'DlzdGD');

-- --------------------------------------------------------

--
-- Table structure for table `firstaid_tbl`
--

CREATE TABLE IF NOT EXISTS `firstaid_tbl` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `fcategory` varchar(100) NOT NULL,
  `fsign` mediumtext NOT NULL,
  `ftreatment` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `firstaid_tbl`
--

INSERT INTO `firstaid_tbl` (`id`, `fcategory`, `fsign`, `ftreatment`) VALUES
(1, 'FAINT(VAS0-VAGAL ATTACK)', '<p>1. Pallor</br> \n2. Cold sweat</br> \n3. Giddiness,Nausea,yawning.</br>\n4. Unsteadiness.</br>\nIf not treated instantly,patient may become unconscious.</p>', '<p>1. Head down between the knees,preferably lie down.</br> \n2. Loosen Clothing.</br>\n3. Warm sweet drink.</br>\nDo not stand up until recovery complete.</p>'),
(2, 'BURNS AND SCALDS', '<p>Burns are caused by fire,electricity,friction,acid,alkali and radiation.</br>\r\nScalds are by hot water,steam</br>\r\n</p>', '<p>\r\n1.Immerse in cold water for 20m. or cold compression.</br>\r\n2.Loose dry dressing.</br>\r\n3.Do not break blister, no touch.</br>\r\n4.Remove wet clothing as it retains heat.</br>\r\n5.Remove wet clothing as it retains heat.</br>\r\n6.Elevate affected part to reduce swelling.</br>\r\n7.Treat shock if any</br>\r\n8.Give small quantities of fluid and pain killer</br>\r\n</p>\r\n'),
(3, 'SNAKE BITE', '<p>\n1.Poisonous snake bite has two tooth marks.</br></br>\n2.Rapid swelling,blistering,spontaneous,haemorrhage,dropping of upper eye lid are features of poisoning.\n</p>', '<p>\n1.Immobilize affected part.</br></br>\n2.Apply tourniquet above bite site free for 30 seconds every half hourly.</br></br>\n3.Wash, use antiseptic cream and cover with gauze.</br></br>\n4.No incision, pricking at site.</br></br>\n5.If snake is killed,bring it or photo to hospital for identification.</br>Do not kill time for catching or killing snake.</br></br>\n6.Ice may be given at site</br></br>\n7.Give tetanus toxoid</br></br>\n8.Treat shock and bring to hospital as soon as possible.\n</p>');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_tbl`
--

CREATE TABLE IF NOT EXISTS `medicine_tbl` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  `disease` varchar(1000) NOT NULL,
  `generic` varchar(1000) NOT NULL,
  `trade` varchar(5000) NOT NULL,
  `forms` varchar(5000) NOT NULL,
  `dosage` varchar(10000) NOT NULL,
  `indication` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `medicine_tbl`
--

INSERT INTO `medicine_tbl` (`id`, `category`, `disease`, `generic`, `trade`, `forms`, `dosage`, `indication`) VALUES
(2, 'Cardio-vascular System', 'Anti-Hypertensive', 'Losartan Potassium', 'ANGLOCK Tab. Square', 'Tablet', 'The usual starting dose is 50mg once daily.', 'For the treatment of Hypertension and Heart failure.'),
(3, 'Gastro-intestinal', 'Antiulcerant', 'Ranitidine', '<p>Neotack-Square</br>Zantac-GlaxoSmith Kline</br>Xantid-ACI</br>Ranidin</br></p>', '<p>Tab.-150mg</br>Inj.-50mg/2ml</br>Syp-100ml bottle</br></p>', 'Tab.-1tab twice daily.', '<p>Gastric ulcer</br>Duodenalucer</br>Reflux Oesophagitis</p>'),
(4, 'Gastro-intestinal', 'Antiulcerant', 'Omeprazole', '<p>\nSquare</br> \nSeclo</br> \nHealthcare</br>\nOpal</br>\nInceptra</br> \nOmenix</br>\nAristopharma</br> \nOmep</br>\nOpsonin</br> \nOmetid</br>\n</p>', '<p>Cap-20mg</br>inj-50mg/ampule</p>', '<p>Two times daily 30min before meal</p>', '<p>Duodenal</p>'),
(5, 'Gastro-intestinal', 'Anti-Diarrhoeal', 'Loperamide HCL', '<p>\r\nSquare</br>\r\nImotil</br>\r\nAcme</br>\r\nLopamid</br>\r\nOpsonin</br>\r\nLoperin</br>\r\n</p>', 'Cap-2mg', 'Initially 2 Cap then 1 Cap at each loose stool.', 'Diarrohoea'),
(6, 'Cardio-vascular System', 'Anti-Hypertensive', 'Methyldopa', '<p>\r\nIncepta</br>\r\nSardopa</br>\r\n</p>', 'Tab-250mg', '2 or 3 times daily', 'Hypertension'),
(7, 'Gastro-intestinal', 'Antiulcerent', 'Esomeprazole', '<p>\r\nSquare</br>\r\nNexum</br>\r\nSergel</br>\r\nIncepta</br>\r\nEsonix</br>\r\nRenata</br>\r\nMaxpro</br>\r\n</p>', 'Cap-20mg Tab-20mg', '2 times daily', '<p>Heart burn,Gasto esophageal Reflux.</p>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
