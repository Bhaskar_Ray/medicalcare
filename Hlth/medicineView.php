
<html>
<head>
<meta charset="UTF-8">
<title>Medical Clinic</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="JavaScript1.1">
<!--



var slideimages=new Array()
var slidelinks=new Array()
function slideshowimages(){
for (i=0;i<slideshowimages.arguments.length;i++){
slideimages[i]=new Image()
slideimages[i].src=slideshowimages.arguments[i]
}
}

function slideshowlinks(){
for (i=0;i<slideshowlinks.arguments.length;i++)
slidelinks[i]=slideshowlinks.arguments[i]
}

function gotoshow(){
if (!window.winslide||winslide.closed)
winslide=window.open(slidelinks[whichlink])
else
winslide.location=slidelinks[whichlink]
winslide.focus()
}

//-->
</script>
</head>
<body>
<div id="main_container">
  <div class="header">
    <div id="logo"><a href=""><img src="images/redCross.jpg" alt="" width="100" height="80" border="0" /></a>
	
	</div>
    <div class="right_header">
	<div class="top_menu"> <a href="http://localhost/Hlth/loginPerform.php" class="login">Admin</a> <a href="" class="sign_up">Contact</a> </div>
      
      <div id="menu">
        <ul>
          <li><a href="http://localhost/Hlth/homePage.php">Home</a></li>
          <li><a href="http://localhost/Hlth/doctorView.php">Doctors</a></li>
          <li><a href="http://localhost/Hlth/donorView.php">Blood</a></li>
		  <li><a href="http://localhost/Hlth/firstAidView.php">First Aid</a></li>
          <li><a href="http://localhost/Hlth/medicineView.php">Medicine</a></li>
          <li><a href="http://localhost/Hlth/ambulanceView.php">Ambulance</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div id="middle_box">
   <a href="javascript:gotoshow()"><img src="food1.jpg" name="slide" border=0 width=900 height=208></a>
<script>
<!--


slideshowimages("images/banner1.jpg","images/banner2.jpg","images/banner3.jpg","images/banner4.jpg","images/banner5.jpg","images/banner6.jpg","images/banner7.jpg","images/banner10.jpg","images/banner9.jpg")
slideshowlinks("","")


var slideshowspeed=2000

var whichlink=0
var whichimage=0
function slideit(){
if (!document.images)
return
document.images.slide.src=slideimages[whichimage].src
whichlink=whichimage
if (whichimage<slideimages.length-1)
whichimage++
else
whichimage=0
setTimeout("slideit()",slideshowspeed)
}
slideit()

//-->
</script>
  </div>
  <div class="pattern_bg">
	
  </div>
  <div id="main_content">
    <div class="box_content">
		<form name="medicineView" action="medicineFinder.php" onsubmit="return ValidInfo()" method="post">

	
					<table>
				
					<tbody>
					<tr>
					
					<td>Drugs used in:</td>
					<td>
					
					
					<select name="category">
					  <option value="Gastro-intestinal">Gastro-intestinal</option>
					  <option value="Cardio-vascular System">Cardio-vascular System</option>
					  <option value="Respiratory diseases">Respiratory diseases</option>
					  <option value="CNS diseases">CNS diseases</option>
					  <option value="Endocrine diseases">Endocrine diseases</option>
					  <option value="Systemic Antimicrobial">Systemic Antimicrobial</option>
					  <option value="Analgesics & Antipyretics">Analgesics & Antipyretics</option>
					  <option value="Anti-rheumatic & Anti-inflammatory">Anti-rheumatic & Anti-inflammatory</option>
					  <option value="Anti-rheumatic & Anti-inflammatory">Allergic disorder</option>
					  <option value="Skin">Skin</option>
					  <option value="Anaemias & some other specific blood disorder">Anaemias & some other specific blood disorder</option>
					  <option value="Vitamin,Mineral & Nutritional deficiency disorders">Vitamin,Mineral & Nutritional deficiency disorders</option>
					  <option value="Water & Electrolytes replacement & plasma substitutes">Water & Electrolytes replacement & plasma substitutes</option>
					  <option value="Bone formation & Bone disorders">Bone formation & Bone disorders</option>
					  <option value="Carcinochemotherapeutic drugs & Cytotoxic Immunosuppressants">Carcinochemotherapeutic drugs & Cytotoxic Immunosuppressants</option>
					  <option value="Immunological products & Vaccines">Immunological products & Vaccines</option>
					   <option value="Anaesthetic & Muscle relaxants">Anaesthetic & Muscle relaxants</option>
					  <option value="Urogenital system">Urogenital system</option>
					  <option value="Ophthalmic problems">Ophthalmic problems</option>
					  <option value="E.N.T problems">E.N.T problems</option>
					  
					</select>
					
					</td>
					
					</tr>
					<tr>
					
					<td><input type="checkbox" name="classDrug" value="location">Class of DRUG:</td>
					<td>
					
					
					<select name="disease">
					  <option value="Antiulcerant">Antiulcerant</option>
					 <option value="Anti-Diarrhoeal">Anti-Diarrhoeal</option>
					  <option value="Anti-Hypertensive">Anti-Hypertensive</option>
					  <option value="Therapy for H.pylori(Triple Therapy)">Therapy for H.pylori(Triple Therapy)</option>
					  <option value="Laxatives,Purgatives,Lubricants">Laxatives,Purgatives,Lubricants</option>
					  <option value="Bita bloker">Bita bloker</option>
					  <option value="">Angiotensin Converting enzyme(ACE) Inhibitor</option>
					  <option value=""></option>
					  <option value=""></option>
					  <option value=""></option>
					  <option value=""></option>
					  <option value=""></option>
					  <option value=""></option>
					  
					  
					</select>
					
					</td>
					
					</tr>
						<tr>
					
					<td><input type="checkbox" name="genName" value="location">Generic Name:</td>
					<td>
					
					
					<select name="generic">
					  <option value="Rabeprazole">Rabeprazole</option>
					  <option value="Loperamide HCL">Loperamide HCL</option>
					  <option value="Nitazoxamide">Nitazoxamide</option>
					  <option value="Losartan Potassium">Losartan Potassium</option>
					  <option value="Ranitidine">Ranitidine</option>
					  <option value="Omeprazole">Omeprazole</option>
					  <option value="Methyldopa">Methyldopa</option>
					  <option value="Esomeprazole">Esomeprazole</option>
					  <option value="">Partoparazole</option>
					  <option value="Lansoprazole+Amoxycillin+clamithromycin">Lansoprazole+Amoxycillin+clamithromycin</option>
					  <option value="Ispaghula Husk">Ispaghula Husk</option>
					  <option value="Lactulose">Lactulose</option>
					  <option value="Milk of magnesia">Milk of magnesia</option>
					  <option value="Didoxin">Didoxin</option>
					  <option value="Propranolol">Propranolol</option>
					  <option value="Atenolol">Atenolol</option>
					  <option value="Metoprolol">Metoprolol</option>
					  <option value="Captopril">Captopril</option>
					  <option value="Ramipril">Ramipril</option>
					  
					  
					</select>
					
					</td>
					
					</tr>
					
					
					<tr>
					
					<td>SEARCH Medicine:</td>
					<td><input name="submit" type="submit" value="OK" id="button2"/></td>
					
					</tr>
					
				
					
					
					</tbody>
					</table>
				
				</form>
			
			
    </div>
   
    
    <div class="clear"></div>
  </div>
  <div id="footer">
   
    <div class="center_footer"><h3>&copy;Health and Medical Care & Blood Bank Web Portal</h3></div>
    
  </div>
</div>
<div align=center> <a href=""></a></div>
<script type="text/javascript">
				function  ValidInfo()
				{
					var a=document.forms["medicineView"]["category"].value;
					
					
					
					
					if (a==null||a=="")
					{
						alert("Category must be filled out.")
						return false;
					
					
					}
						
						
						
						
									
									
									
									
									
				}
			</script>
				

</body>
</html>
